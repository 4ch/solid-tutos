import React, { useEffect, useState } from 'react';
import {
  getSolidDataset,
  getThingAll,
  getUrl,
  getSourceUrl,
  getStringNoLocale,
  setStringNoLocale,
  removeThing,
  saveSolidDatasetAt } from "@inrupt/solid-client";
import {
  Table,
  TableColumn,
  useThing,
} from "@inrupt/solid-ui-react";
import { SKOS, RDF, FOAF } from "@inrupt/vocab-common-rdf";
import IconButton from '@mui/material/IconButton';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import Showdown from "showdown"
import ReactHtmlParser from 'react-html-parser';

const RDF_TYPE = RDF.type
const SKOS_CONCEPT = SKOS.Concept
const SKOS_PREFLABEL = SKOS.prefLabel
const SKOS_ALTLABEL = SKOS.altLabel
const SKOS_DEFINITION = SKOS.definition
const FOAF_HOMEPAGE = FOAF.homepage

function DeleteButton({ deleteTerm }) {
  const { thing } = useThing();
  return (
    <IconButton onClick={() => deleteTerm(thing)}>
      <DeleteForeverIcon />
    </IconButton>
  );
}

export default function ListTerm(props){
//  console.log("ListTerm/props=",props);

  const { session, uriGlossary, termList, goSetTermList } = props;
  const [loading, setLoading] = useState(true)

  // Markdown to Html for altLabel
  const converter = new Showdown.Converter();

  const deleteTerm = async (term) => {
    if (!session || !session.info.isLoggedIn)
    {
      document.getElementById("message").textContent = "Please log before...";
      return;
    }
    const termsUrl = getSourceUrl(termList);
    const removedTerm = removeThing(termList, term);
    const updatedDataset = await saveSolidDatasetAt(termsUrl, removedTerm, {
      fetch: session.fetch,
    });
    goSetTermList(updatedDataset);
  };

  useEffect(() => {
    // if (!session || !session.info.isLoggedIn) return;
    (async () => {
      const termList = await getSolidDataset(uriGlossary, {
        fetch: session.fetch
      });
      goSetTermList(termList);
      setLoading(false)
    })();
  }, [termList]);

  const termThings = termList ? getThingAll(termList) : [];

  termThings.forEach( (term, index, table) => {
    const skosPreflabel = getStringNoLocale(term, SKOS_PREFLABEL)
    const foafHomepage = getUrl(term, FOAF_HOMEPAGE)
    const htmlLink = "<a href="+foafHomepage+" target='blank'>"+skosPreflabel+"</a>"
    table[index] = setStringNoLocale(term, SKOS_PREFLABEL, htmlLink)
  })

  const thingsArray = termThings
     .filter( (t) => {
       const url = getUrl(t, RDF_TYPE)
       return url === SKOS_CONCEPT
     })
     .map((t) => {
       return { dataset: termList, thing: t };
     });

  if (loading)
    return "Loading..."

  return (
    <>
      <Table className="table" things={thingsArray}>
        <TableColumn
          property={SKOS_PREFLABEL}
          header="Terme"
          sortable
          body= {( {value} ) => {
            const prefLabelHtml = converter.makeHtml(value);
            return <div>{ ReactHtmlParser(prefLabelHtml) }</div>;
          }}
        />
        <TableColumn
          property={SKOS_ALTLABEL}
          header="Signification"
          sortable
          body= {( {value} ) => {
            const altLabelHtml = converter.makeHtml(value);
            return <div>{ ReactHtmlParser(altLabelHtml) }</div>;
          }}
        />
        <TableColumn
          property={SKOS_DEFINITION}
          header="Description"
          sortable
        />
        <TableColumn
          property={SKOS_PREFLABEL}
          header="X"
          body={() => <DeleteButton deleteTerm={deleteTerm} />}
        />
      </Table>
    </>
    )
}
