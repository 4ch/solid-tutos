import React, { useState } from 'react';
import { useForm } from "react-hook-form";
import IconButton from '@mui/material/IconButton';
import RefreshIcon from '@mui/icons-material/Refresh';
import styled from 'styled-components'

export default function ChooseGlossary(props) {
//  console.log("chooseGlossary/props=",props);
  // CSS adaptation
  const P1 = styled.p`
    margin : 0px;
  `;
  const HighInput = styled.input`
    width: auto;
    display: inline-block;
    line-height: 2;
  `;

  const { register, handleSubmit, formState: { errors }, setValue } = useForm({
    defaultValues : { uriInputGlossary : props.uriGlossary }
  });

  const onSubmit = (data) => {
    console.log("onSubmit/data=",data)
    props.goSetUriGlossary(data.uriInputGlossary);
    props.goSetReadyToFetchGlossary();
    props.goSetReadyToDisplayEditForm();
  };

  const [selectedOption, setSelectedOption] = useState(props.options[0].uri);

  const onChange = (e) => {
    console.log("onChange/ e.target.value=",e.target.value)
    // Set Selected option
    setSelectedOption(e.target.value)
    // Set input with uri of the select field
    setValue("uriInputGlossary", e.target.value)
  }

  const refreshSubmit = () => {
    //console.log("refreshSubmit / props.options[0].uri=",props.options[0].uri)
    // form input initialisation
    setValue("uriInputGlossary", props.options[0].uri);
    // select initialisation to glossary0
    setSelectedOption(props.options[0].value)
  }

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <HighInput type='text' { ...register("uriInputGlossary") } size="50"/>

        <IconButton aria-label="delete" onClick={refreshSubmit}>
          <RefreshIcon />
        </IconButton>

        <P1>Ou de sélectionner un des Lexiques ci-dessous</P1>
        <select id="selectedUriGlossary" {...register("selectedUriGlossary")}
            value={selectedOption}
            onChange={onChange}
            width="50">
        {props.options.map((item) => (
          <option key={item.value} value={item.uri}>
            {item.uri}
          </option>
        ))}
        </select>

        <input type="submit" value="Lister les termes du lexique"></input>

        <p className="has-error">{errors.uriInput?.message}</p>
      </form>
    </>
  );
}
